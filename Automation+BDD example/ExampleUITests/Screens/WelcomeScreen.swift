//
//  WelcomeScreen.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/25/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest
import UIKit

class WelcomeScreen: Screen {
    
    let app: XCUIApplication
    let debugName = "WelcomeScreenViewController"
    let visibilityQuery: XCUIElementQuery
    
    let signIn: Button
    
    init(app: XCUIApplication) {
        self.app = app
        
        visibilityQuery = app.otherElements.matching(identifier: debugName)
        
        signIn = Button(app: app, debugName: debugName, query: app.buttons.matching(identifier: "SignInButton"))
    }
}
