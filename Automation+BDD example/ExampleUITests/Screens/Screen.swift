//
//  Screen.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/25/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest
import UIKit

protocol Screen {
    var debugName: String { get }
    var app: XCUIApplication { get }
    var visibilityQuery: XCUIElementQuery { get }
}

extension Screen {
    
    func waitToAppear(timeout: TimeInterval = InterfaceItem.defaultTimeout, file: StaticString = #file, line: UInt = #line) {
        
        let element = visibilityQuery.element
        
        let predicate = NSPredicate(format: "exists == true")
        let expectedResult = XCTNSPredicateExpectation(predicate: predicate, object: element)
        let result = XCTWaiter().wait(for: [expectedResult], timeout: timeout)
        
        switch result {
        case .completed:
            return
        case .timedOut:
            XCTFail("Timed out waiting for screen to appear: \(debugName)", file: file, line: line)
        case .incorrectOrder:
            XCTFail("waitToAppear XCTWaiter returned .incorrectOrder result (shouldn't happen)", file: file, line: line)
        case .invertedFulfillment:
            XCTFail("waitToAppear XCTWaiter returned .invertedFulfillment result (shouldn't happen)", file: file, line: line)
        case .interrupted:
            XCTFail("waitToAppear XCTWaiter returned .interrupted result (shouldn't happen)", file: file, line: line)
        }
    }
    
    func waitToDisappear(timeout: TimeInterval = InterfaceItem.defaultTimeout, file: StaticString = #file, line: UInt = #line) {
        
        let element = visibilityQuery.element
        
        if element.exists == false {
            return
        }
        
        let predicate = NSPredicate(format: "exists == false")
        let expectedResult = XCTNSPredicateExpectation(predicate: predicate, object: element)
        let result = XCTWaiter().wait(for: [expectedResult], timeout: timeout)
        
        switch result {
        case .completed:
            return
        case .timedOut:
            XCTFail("Timed out waiting for screen to disappear: \(debugName)", file: file, line: line)
        case .incorrectOrder:
            XCTFail("waitToAppear XCTWaiter returned .incorrectOrder result (shouldn't happen)", file: file, line: line)
        case .invertedFulfillment:
            XCTFail("waitToAppear XCTWaiter returned .invertedFulfillment result (shouldn't happen)", file: file, line: line)
        case .interrupted:
            XCTFail("waitToAppear XCTWaiter returned .interrupted result (shouldn't happen)", file: file, line: line)
        }
    }
}
