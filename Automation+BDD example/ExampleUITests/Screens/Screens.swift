//
//  Screens.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/24/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest

class Screens {
    
    let welcomeScreen: WelcomeScreen

    init(hostApplication: XCUIApplication,
         safariApplication: XCUIApplication,
         settingsApplication: XCUIApplication,
         testCase: XCTestCase) {

        welcomeScreen = WelcomeScreen(app: hostApplication)
    }
}
