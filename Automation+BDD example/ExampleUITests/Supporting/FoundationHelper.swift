//
//  FoundationHelper.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/25/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation

public func repeated(times: Int, handler: () -> Void) {
    assert(times.isPositive, "Times must not be negative")
    
    if times <= 0 {
        return
    }
    
    for _ in (0..<times) {
        handler()
    }
}

public func NotImplemented() -> Never {
    fatalError("Not Implemented")
}

public extension Numeric where Self: Comparable {
    
    var isNegative: Bool {
        return self < 0
    }
    
    var isPositive: Bool {
        return self >= 0
    }
}
