//
//  TextField.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/25/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest

class TextField: InterfaceItem {
    
    func populate(_ text: String, shouldTap: Bool = true, dismissKeyboard: Bool = false, file: StaticString = #file, line: UInt = #line) {
        
        waitToAppear(file: file, line: line)
        let textField = self.element
        
        if shouldTap {
            textField.tap()
        }
        
        // Empty textfield
        if let stringValue = textField.value as? String, stringValue.count > 0 {
            var deleteString = String()
            repeated(times: stringValue.count) {
                deleteString += XCUIKeyboardKey.delete.rawValue
            }
            textField.typeText(deleteString)
        }
        
        textField.typeText(text)
        
        if dismissKeyboard {
            textField.typeText("\n")
        }
    }
    
    func assert(text: String, file: StaticString = #file, line: UInt = #line) {
        
        if let value = element.value as? String {
            if value != text {
                XCTFail("Textfield \(debugName) contains \"\(value)\", expected \"\(text)\"", file: file, line: line)
            }
        } else {
            XCTFail("Textfield \(debugName) contains no value, expected \"\(text)\"", file: file, line: line)
        }
    }
}
