//
//  Button.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/25/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest

class Button: InterfaceItem {
    
    func assertEnabled(file: StaticString = #file, line: UInt = #line) {
        XCTAssertTrue(element.isEnabled, "Expected \(debugName) to be enabled, but was disabled", file: file, line: line)
    }
    
    func assertDisabled(file: StaticString = #file, line: UInt = #line) {
        XCTAssertFalse(element.isEnabled, "Expected \(debugName) to be disable, but was enabled", file: file, line: line)
    }
    
    func assertSelected(_ selected: Bool, file: StaticString = #file, line: UInt = #line) {
        let assertionMessage = selected ? "selected" : "deselected"
        let elementSelected = element.isSelected ? "selected" : "deselected"
        XCTAssertEqual(selected, element.isSelected, "Expected \(debugName) to be \(assertionMessage), but was \(elementSelected)", file: file, line: line)
    }
    
    func waitToBeEnabled(timeout: TimeInterval = InterfaceItem.defaultTimeout, file: StaticString = #file, line: UInt = #line) {
        
        if element.isEnabled {
            return
        }
        
        let predicate = NSPredicate(format: "isEnabled == true")
        let expectedResult = XCTNSPredicateExpectation(predicate: predicate, object: element)
        let result = XCTWaiter().wait(for: [expectedResult], timeout: timeout)
        
        switch result {
        case .completed:
            return
        case .timedOut:
            XCTFail("Expected \(debugName) to be enabled, but was disabled", file: file, line: line)
        case .incorrectOrder:
            XCTFail("waitToAppear XCTWaiter returned .incorrectOrder result (shouldn't happen)", file: file, line: line)
        case .invertedFulfillment:
            XCTFail("waitToAppear XCTWaiter returned .invertedFulfillment result (shouldn't happen)", file: file, line: line)
        case .interrupted:
            XCTFail("waitToAppear XCTWaiter returned .interrupted result (shouldn't happen)", file: file, line: line)
        }
    }
    
    func waitToBeDisabled(timeout: TimeInterval = InterfaceItem.defaultTimeout, file: StaticString = #file, line: UInt = #line) {
        
        if !element.isEnabled {
            return
        }
        
        let predicate = NSPredicate(format: "isEnabled == false")
        let expectedResult = XCTNSPredicateExpectation(predicate: predicate, object: element)
        let result = XCTWaiter().wait(for: [expectedResult], timeout: timeout)
        
        switch result {
        case .completed:
            return
        case .timedOut:
            XCTFail("Expected \(debugName) to be disable, but was enabled", file: file, line: line)
        case .incorrectOrder:
            XCTFail("waitToAppear XCTWaiter returned .incorrectOrder result (shouldn't happen)", file: file, line: line)
        case .invertedFulfillment:
            XCTFail("waitToAppear XCTWaiter returned .invertedFulfillment result (shouldn't happen)", file: file, line: line)
        case .interrupted:
            XCTFail("waitToAppear XCTWaiter returned .interrupted result (shouldn't happen)", file: file, line: line)
        }
    }
    
    override func tap(timeout: TimeInterval = InterfaceItem.defaultTimeout, file: StaticString = #file, line: UInt = #line) {
        waitToAppear(timeout: timeout, file: file, line: line)
        waitToBeEnabled(timeout: timeout, file: file, line: line)
        element.tap()
    }
}
