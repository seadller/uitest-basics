//
//  InterfaceItem.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/25/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest

class InterfaceItem {
    
    static let defaultTimeout = TimeInterval(10.0)
    
    let app: XCUIApplication
    let query: XCUIElementQuery
    var elementIndex = 0
    let debugName: String
    
    var element: XCUIElement {
        return query.element(boundBy: elementIndex)
    }
    
    init(app: XCUIApplication, debugName: String, query: XCUIElementQuery) {
        self.app = app
        self.query = query
        self.debugName = debugName
    }
    
    func waitToAppear(timeout: TimeInterval = InterfaceItem.defaultTimeout, file: StaticString = #file, line: UInt = #line) {
        
        if element.exists {
            return
        }
        
        let predicate = NSPredicate(format: "exists == true")
        let expectedResult = XCTNSPredicateExpectation(predicate: predicate, object: element)
        let result = XCTWaiter().wait(for: [expectedResult], timeout: timeout)
        
        switch result {
        case .completed:
            return
        case .timedOut:
            XCTFail("Timed out waiting for element to appear: \(debugName)", file: file, line: line)
        case .incorrectOrder:
            XCTFail("waitToAppear XCTWaiter returned .incorrectOrder result (shouldn't happen)", file: file, line: line)
        case .invertedFulfillment:
            XCTFail("waitToAppear XCTWaiter returned .invertedFulfillment result (shouldn't happen)", file: file, line: line)
        case .interrupted:
            XCTFail("waitToAppear XCTWaiter returned .interrupted result (shouldn't happen)", file: file, line: line)
        }
    }
    
    func waitToDisappear(timeout: TimeInterval = InterfaceItem.defaultTimeout, file: StaticString = #file, line: UInt = #line) {
        
        if element.exists == false {
            return
        }
        
        let predicate = NSPredicate(format: "exists == false")
        let expectedResult = XCTNSPredicateExpectation(predicate: predicate, object: element)
        let result = XCTWaiter().wait(for: [expectedResult], timeout: timeout)
        
        switch result {
        case .completed:
            return
        case .timedOut:
            XCTFail("Timed out waiting for element to disappear: \(debugName)", file: file, line: line)
        case .incorrectOrder:
            XCTFail("waitToAppear XCTWaiter returned .incorrectOrder result (shouldn't happen)", file: file, line: line)
        case .invertedFulfillment:
            XCTFail("waitToAppear XCTWaiter returned .invertedFulfillment result (shouldn't happen)", file: file, line: line)
        case .interrupted:
            XCTFail("waitToAppear XCTWaiter returned .interrupted result (shouldn't happen)", file: file, line: line)
        }
    }
    
    func tap(timeout: TimeInterval = InterfaceItem.defaultTimeout, file: StaticString = #file, line: UInt = #line) {
        waitToAppear(timeout: timeout, file: file, line: line)
        element.tap()
    }
}
