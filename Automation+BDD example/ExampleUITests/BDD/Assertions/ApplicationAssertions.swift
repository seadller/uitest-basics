//
//  ApplicationAssertions.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/24/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest

class ApplicationAssertions {
    
    private let screens: Screens
    private let safari: XCUIApplication
    
    init(screens: Screens, safari: XCUIApplication) {
        self.screens = screens
        self.safari = safari
    }
    
    func seesSafari(file: StaticString = #file, line: UInt = #line) {
        XCTAssert(safari.wait(for: .runningForeground, timeout: 10), "The safari application was never launched", file: file, line: line)
    }
}
