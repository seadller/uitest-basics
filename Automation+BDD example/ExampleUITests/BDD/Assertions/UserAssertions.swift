//
//  UserAssertions.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/24/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import UIKit

class UserAssertions {
    
    private let screens: Screens
    
    init(screens: Screens) {
        self.screens = screens
    }
    
    @discardableResult func onWelcomeScreen(file: StaticString = #file, line: UInt = #line) -> Any {
        screens.welcomeScreen.waitToAppear(file: file, line: line)
        return UserAssertions(screens: screens)
    }
}
