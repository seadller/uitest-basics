//
//  ApplicationActions.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/25/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation

class ApplicationActions {
    
    private let screens: Screens
    
    init(screens: Screens) {
        self.screens = screens
    }
}
