//
//  UserActions.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/24/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest

class UserActions {
    
    private let screens: Screens
    
    init(screens: Screens) {
        self.screens = screens
    }

    @discardableResult func onWelcomeScreen() -> WelcomeActions {
        return WelcomeActions(screens: screens)
    }
}

