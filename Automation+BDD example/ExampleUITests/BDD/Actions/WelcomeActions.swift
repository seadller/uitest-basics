//
//  WelcomeActions.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/25/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation

class WelcomeActions {
    
    private let screens: Screens
    
    init(screens: Screens) {
        self.screens = screens
    }
    
    @discardableResult func navigatesToSignInScreen(file: StaticString = #file, line: UInt = #line) -> WelcomeActions {
        screens.welcomeScreen.signIn.tap(file: file, line: line)
        return self
    }
}
