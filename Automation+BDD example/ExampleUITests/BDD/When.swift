//
//  When.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/24/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation

class When {
    
    let user: UserActions
    let app: ApplicationActions

    init(screens: Screens) {
        user = UserActions(screens: screens)
        app = ApplicationActions(screens: screens)
    }
}
