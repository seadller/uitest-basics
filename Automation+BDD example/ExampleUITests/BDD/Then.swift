//
//  Then.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/24/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest

class Then {
    
    let user: UserAssertions
    let app: ApplicationAssertions
    
    init(screens: Screens, safari: XCUIApplication) {
        self.user = UserAssertions(screens: screens)
        self.app = ApplicationAssertions(screens: screens, safari: safari)
    }
}
