//
//  AuthServiceArrangement.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/25/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest

class AuthServiceArrangement {
    
    private let app: XCUIApplication
    
    init(app: XCUIApplication) {
        self.app = app
    }

    func respondsWithInternalError(file: StaticString = #file, line: UInt = #line) {
        //
    }
    
    func respondsWithTimeout(file: StaticString = #file, line: UInt = #line) {
        //
    }
    
    func respondsWithInvalidCredentials(file: StaticString = #file, line: UInt = #line) {
        //
    }
    
    func respondsWithSucess(file: StaticString = #file, line: UInt = #line) {
        //
    }
}
