//
//  NetworkConnectionArrangement.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/25/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest

class NetworkConnectionArrangement {
    
    private let app: XCUIApplication
    
    init(app: XCUIApplication) {
        self.app = app
    }
    
    func noInternetConnection() {
        // 
    }
    
    func stableInternetConnection() {
        //
    }
}
