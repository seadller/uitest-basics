//
//  OnboardingArrangement.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/24/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest

class OnboardingArrangement {
    
    private let app: XCUIApplication
    
    init(app: XCUIApplication) {
        self.app = app
    }
    
    func hasBeenCompleted() {
        app.launchArguments = ["resetAllAppData"]
    }
    
    func askNotificationsPermission() {

    }
    
    func askLocationPermission() {
        
    }
}
