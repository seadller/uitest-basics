//
//  UserArrangement.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/24/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest

class UserArrangement {
    
    private let app: XCUIApplication
    private let screens: Screens
    private let server: MockServer
    private let userBuilder = UserBuilder()

    init(_ app: XCUIApplication, _ screens: Screens, _ server: MockServer) {
        
        self.app = app
        self.screens = screens
        self.server = server
    }
    
    func isLoggedIn(file: StaticString = #file, line: UInt = #line) {
        //
    }
    
    func isLoggedOut(file: StaticString = #file, line: UInt = #line) {
        //
    }
    
    func launchesApp() {
        app.launch()
    }
}
