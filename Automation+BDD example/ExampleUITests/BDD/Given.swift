//
//  Given.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/24/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import XCTest

class Given {
    
    let user: UserArrangement
    let onboarding: OnboardingArrangement
    let networkConnection: NetworkConnectionArrangement
    let authService: AuthServiceArrangement

    init(app: XCUIApplication, screens: Screens, server: MockServer) {
        user = UserArrangement(app,screens, server)
        onboarding = OnboardingArrangement(app: app)
        networkConnection = NetworkConnectionArrangement(app: app)
        authService = AuthServiceArrangement(app: app)
    }
}
