//
//  Server.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/24/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation

protocol MockServer {
    func start()
    func stop()
}

class MockServerImpl: MockServer {
    func start() {
        //
    }
    
    func stop() {
        //
    }
}
