//
//  Automation_BDD_exampleUITests.swift
//  Automation+BDD exampleUITests
//
//  Created by Roman Malinovskyi on 10/24/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import XCTest

class UITestBase: XCTestCase {

    let hostApplication = XCUIApplication()
    let safariApplication = XCUIApplication(bundleIdentifier: "com.apple.mobilesafari")
    let settingsApplication = XCUIApplication(bundleIdentifier: "com.apple.Preferences")

    var given: Given!
    var when: When!
    var then: Then!

    private var screens: Screens!
    private var server: MockServer!
    
    override func setUp() {
        super.setUp()

        server = MockServerImpl()
        server.start()
        
        // Interface Items and Screens
        
        screens = Screens(hostApplication: hostApplication, safariApplication: safariApplication, settingsApplication: settingsApplication, testCase: self)

        //
        
        given = Given(app: hostApplication, screens: screens, server: server)
        when = When(screens: screens)
        then = Then(screens: screens, safari: safariApplication)

        //MARK:- Default setup

        given.user.isLoggedOut()
        given.networkConnection.stableInternetConnection()
        given.authService.respondsWithSucess()
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
    }
    
    override func tearDown() {
        super.tearDown()
        server.stop()
    }
}
