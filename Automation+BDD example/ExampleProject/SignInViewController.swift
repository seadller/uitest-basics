//
//  SignInViewController.swift
//  ExampleUITests
//
//  Created by Roman Malinovskyi on 10/25/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import Foundation
import UIKit

class SignInViewController: UIViewController {
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var login: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.accessibilityIdentifier = "SignInViewController"
        setupTargets()
    }
    
    // MARK:- Setup

    private func setupTargets() {
        isLogin(enabled: false)
        email.addTarget(self, action: #selector(textFieldsIsNotEmpty),
                                     for: .editingChanged)
        password.addTarget(self, action: #selector(textFieldsIsNotEmpty),
                                        for: .editingChanged)
    }
    
    // MARK:- Helpers
    
    private func isLogin(enabled: Bool) {
        login.isUserInteractionEnabled = enabled
        login.isEnabled = enabled
        login.backgroundColor = enabled ? .blue : .gray
    }
    
    @objc func textFieldsIsNotEmpty(sender: UITextField) {
        
        sender.text = sender.text?.trimmingCharacters(in: .whitespaces)
        
        guard let emailText = email.text, !emailText.isEmpty,
            let passwordText = password.text, !passwordText.isEmpty
            else {
                self.isLogin(enabled: false)
            return
        }
        
        isLogin(enabled: true)
    }
}
