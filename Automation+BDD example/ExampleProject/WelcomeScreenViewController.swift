//
//  ViewController.swift
//  Automation+BDD example
//
//  Created by Roman Malinovskyi on 10/24/18.
//  Copyright © 2018 Roman Malinovskyi. All rights reserved.
//

import UIKit

class WelcomeScreenViewController: UIViewController {
    
    @IBOutlet var signIn: UIButton!
    @IBOutlet var signUp: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        view.accessibilityIdentifier = "WelcomeScreenViewController"
        signIn.accessibilityIdentifier = "SignInButton"
    }
}

